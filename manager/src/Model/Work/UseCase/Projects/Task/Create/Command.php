<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\Create;

use App\Model\Work\Entity\Projects\Task\Type;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $project;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $author;

    /**
     * @var NameRow[]
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    public $names;

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $parent;

    /**
     * @var \DateTimeImmutable
     * @Assert\Date()
     */
    public $planDate;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $type;

    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $priority;

    public function __construct(
        string $project,
        string $author
    ) {
        $this->project = $project;
        $this->author = $author;
        $this->type = Type::NONE;
        $this->priority = 2;
    }
}
