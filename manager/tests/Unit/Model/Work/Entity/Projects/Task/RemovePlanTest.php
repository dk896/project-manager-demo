<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\Work\Entity\Projects\Task;

use PHPUnit\Framework\TestCase;
use App\Model\Work\Entity\Projects\Task\Task;
use App\Model\Work\Entity\Projects\Task\Type;
use App\Tests\Builder\Work\Members\GroupBuilder;
use App\Tests\Builder\Work\Members\MemberBuilder;
use App\Tests\Builder\Work\Projects\ProjectBuilder;
use App\Model\Work\Entity\Projects\Task\Id as TaskId;
use App\Tests\Builder\Work\Projects\TaskBuilder;

class RemovePlanTest extends TestCase
{
    public function testSuccess(): void
    {
        $group = (new GroupBuilder())->build();
        $member = (new MemberBuilder())->build($group);
        $project = (new ProjectBuilder())->build();

        $task = (new TaskBuilder())->build($project, $member);

        $task->plan(
            $member,
            new \DateTimeImmutable(),
            $date = new \DateTimeImmutable('+2 day')
        );

        self::assertEquals($date, $task->getPlanDate());

        $task->removePlan($member, new \DateTimeImmutable('+1 day'));

        self::assertNull($task->getPlanDate());
    }
}
