<?php

declare(strict_types=1);

namespace App\Tests\Builder\Work\Members;

use App\Model\Work\Entity\Members\Group\Id as GroupId;
use App\Model\Work\Entity\Members\Group\Group;

class GroupBuilder
{
    private $name;

    public function __construct()
    {
        $this->name = 'Group one';
    }

    public function withName(string $name): self
    {
        $clone = clone $this;
        $clone->name = $name;

        return $clone;
    }

    public function build(): Group
    {
        return new Group(
            GroupId::next(),
            $this->name
        );
    }
}
