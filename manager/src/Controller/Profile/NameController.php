<?php

declare(strict_types=1);

namespace App\Controller\Profile;

use App\Model\User\UseCase\Name;
use App\Controller\ErrorHandler;
use App\ReadModel\User\UserFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/profile/name")
 */
class NameController extends AbstractController
{
    private $users;
    private $errors;

    public function __construct(
        UserFetcher $users,
        ErrorHandler $errors
    ) {
        $this->users = $users;
        $this->errors = $errors;
    }

    /**
     * @Route("", name="profile.name")
     * @param Request $request
     * @param Name\Handler $handler
     * @return Response
     */
    public function edit(
        Request $request,
        Name\Handler $handler
    ): Response {
        $user = $this->users->get($this->getUser()->getId());

        $command = Name\Command::fromUser($user);

        $form = $this->createForm(Name\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);

                return $this->redirectToRoute('profile');
            } catch (\DomainException $err) {
                $this->errors->handle($err);
                $this->addFlash('error', $err->getMessage());
            }
        }

        return $this->render(
            'app/profile/name.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
