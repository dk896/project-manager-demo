<?php

namespace App\Tests\Functional\OAuth;

use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\Email;
use App\Tests\Builder\User\UserBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Trikoder\Bundle\OAuth2Bundle\Model\Grant;
use Doctrine\Common\Persistence\ObjectManager;
use Trikoder\Bundle\OAuth2Bundle\Model\Client;
use Trikoder\Bundle\OAuth2Bundle\OAuth2Grants;
use App\Model\User\Service\PasswordHasher;

class OAuthFixture extends Fixture
{
    public const OAUTH_PASSWORD = 'password#531';
    public const OAUTH_EMAIL = 'oauth-user@app.org';
    public const CLIENT_SECRET = 'secret';
    public const CLIENT_ID = 'oauth';

    private $hasher;

    public function __construct(PasswordHasher $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = (new UserBuilder())
            ->viaEmail(
                new Email(self::OAUTH_EMAIL),
                $this->hasher->hash(self::OAUTH_PASSWORD)
            )
            ->withName(new Name('Oather', 'Oatherson'))
            ->confirmed()
            ->build()
        ;

        $manager->persist($user);

        $client = new Client(self::CLIENT_ID, self::CLIENT_SECRET);
        $client->setGrants(new Grant(OAuth2Grants::PASSWORD));

        $manager->persist($client);

        $manager->flush();
    }
}
