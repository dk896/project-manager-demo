<?php

declare(strict_types=1);

namespace App\Tests\Functional\Users;

use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\Email;
use App\Tests\Builder\User\UserBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UsersFixture extends Fixture
{
    public const EXISTING_ID = '00000000-0000-0000-0000-000000000001';
    public const SHOW_FIRST_NAME = 'Mark';
    public const SHOW_LAST_NAME = 'Dukalskey';

    public function load(ObjectManager $manager): void
    {
        $existingUser = (new UserBuilder())
            ->viaEmail(new Email('existing-user@app.org'), 'hash')
            ->confirmed()
            ->build()
        ;

        $manager->persist($existingUser);

        $showUser = (new UserBuilder())
            ->viaEmail(new Email('mark-dukalskey@app.org'), 'hash')
            ->withName(new Name(self::SHOW_FIRST_NAME, self::SHOW_LAST_NAME))
            ->withId(new Id(self::EXISTING_ID))
            ->confirmed()
            ->build()
        ;

        $manager->persist($showUser);

        $manager->flush();
    }
}
