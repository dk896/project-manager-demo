<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Reset\Reset;

use App\Model\Flusher;
use App\Model\User\Service\PasswordHasher;
use App\Model\User\Entity\User\UserRepository;

class Handler
{
    /** @var UserRepository */
    private $users;

    /** @var PasswordHasher */
    private $hasher;

    /** @var Flusher */
    private $flusher;

    public function __construct(
        UserRepository $users,
        PasswordHasher $hasher,
        Flusher $flusher
    ) {
        $this->users = $users;
        $this->hasher = $hasher;
        $this->flusher = $flusher;
    }

    /**
     * Method for handle user's command DTO
     *
     * @param Command $command DTO
     * @return void
     **/
    public function handle(Command $command): void
    {
        /** @var App\Model\User\Entity\User\User $user */
        if (
            !$user = $this->users->findByResetToken(
                $command->token
            )
        ) {
            throw new \DomainException('Incorrect confirmed token.');
        }

        $user->passwordReset(
            new \DateTimeImmutable(),
            $this->hasher->hash($command->password)
        );

        $this->flusher->flush();
    }
}
