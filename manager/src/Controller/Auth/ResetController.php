<?php

declare(strict_types=1);

namespace App\Controller\Auth;

use App\Controller\ErrorHandler;
use App\Model\User\UseCase\Reset;
use App\ReadModel\User\UserFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ResetController extends AbstractController
{
    private $errors;

    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/reset", name="auth.reset")
     * @param Request $request
     * @param Reset\Request\Handler $handler
     * @return Response
     */
    public function request(
        Request $request,
        Reset\Request\Handler $handler
    ): Response {
        $command = new Reset\Request\Command();

        $form = $this->createForm(
            Reset\Request\Form::class,
            $command
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->addFlash('success', 'Check your email for information.');

                return $this->redirectToRoute('home');
            } catch (\DomainException $error) {
                $this->errors->handle($error);
                $this->addFlash('error', $error->getMessage());
            }
        }

        return $this->render('app/auth/reset/request.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/reset/{token}", name="auth.reset.reset")
     * @param string $token
     * @param Request $request
     * @param Reset\Reset\Handler $handler
     * @param UserFetcher $users
     * @return Response
     */
    public function reset(
        string $token,
        Request $request,
        Reset\Reset\Handler $handler,
        UserFetcher $users
    ): Response {
        if (!$users->existsByResetToken($token)) {
            $this->addFlash('error', 'Incorrenct or already confirmed token.');

            return $this->redirectToRoute('home');
        }

        $command = new Reset\Reset\Command($token);

        $form = $this->createForm(Reset\Reset\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->addFlash('success', 'Password was changed successfully.');

                return $this->redirectToRoute('home');
            } catch(\DomainException $error) {
                $this->errors->handle($error);

                $this->addFlash('error', $error->getMessage());

                return $this->redirectToRoute('home');
            }
        }

        return $this->render('app/auth/reset/reset.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
