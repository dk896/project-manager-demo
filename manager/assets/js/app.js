require('../css/app.scss');

require('bootstrap');
require('@coreui/coreui');

const Centrifuge = require('centrifuge');
const toastr = require('toastr');

document.addEventListener('DOMContentLoaded', function () {
    const url = document.querySelector('meta[name="centrifugo-url"]').getAttribute('content');
    const user = document.querySelector('meta[name="centrifugo-user"]').getAttribute('content');
    const token = document.querySelector('meta[name="centrifugo-token"]').getAttribute('content');
    const centrifuger = new Centrifuge(url);

    centrifuger.setToken(token);

    centrifuger.subscribe('alerts#' + user, function (message) {
        toastr.info(message.data.message);
    });

    centrifuger.connect();
});
