<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Members\Member\Move;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\ReadModel\Work\Members\GroupFetcher;

class Form extends AbstractType
{
    private $groups;

    public function __construct(GroupFetcher $groups)
    {
        $this->groups = $groups;
    }

    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add(
                'group',
                Type\ChoiceType::class,
                [
                    'choices' => array_flip($this->groups->assoc()),
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => Command::class,
            ]);
    }
}
