<?php

declare(strict_types=1);

namespace App\Model\User\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\Role;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(
 *  name="user_users",
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"email"}),
*       @ORM\UniqueConstraint(columns={"reset_token_token"})
 * })
 */
class User
{
    public const STATUS_WAIT =    'wait';
    public const STATUS_ACTIVE =  'active';
    public const STATUS_BLOCKED = 'blocked';

    /**
     * @ORM\Column(type="user_user_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;

    /**
     * @var Email|null
     * @ORM\Column(type="user_user_email", nullable=true)
     */
    private $email;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="password_hash", nullable=true)
     */
    private $passwordHash;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="confirm_token", nullable=true)
     */
    private $confirmToken;

    /**
     * @var Name
     * @ORM\Embedded(class="Name")
     */
    private $name;

    /**
     * @var ResetToken|null
     * @ORM\Embedded(class="ResetToken", columnPrefix="reset_token_")
     */
    private $resetToken;

    /**
     * @var string
     * @ORM\Column(type="string", length=16)
     */
    private $status;

    /**
     * @var Role
     * @ORM\Column(type="user_user_role", length=16)
     */
    private $role;

    /**
     * @var Network[]|ArrayCollection
     * @ORM\OneToMany(
     *  targetEntity="Network",
     *  mappedBy="user",
     *  orphanRemoval=true,
     *  cascade={"persist"}
     * )
     */
    private $networks;

    /**
     * @var Email|null
     * @ORM\Column(
     *  type="user_user_email",
     *  name="new_email",
     *  nullable=true
     * )
     */
    private $newEmail;

    /**
     * @var string|null
     * @ORM\Column(
     *  type="string",
     *  name="new_email_token",
     *  nullable=true
     * )
     */
    private $newEmailToken;

    /**
     * @ORM\Version()
     * @ORM\Column(type="integer")
     */
    private $version;

    private function __construct(
        Id $id,
        \DateTimeImmutable $date,
        Name $name
    ) {
        $this->id = $id;
        $this->date = $date;
        $this->name = $name;
        $this->role = Role::user();
        $this->networks = new ArrayCollection();
    }

    public static function create(
        Id $id,
        \DateTimeImmutable $date,
        Name $name,
        Email $email,
        string $hash
    ): self {
        $user = new self($id, $date, $name);
        $user->email = $email;
        $user->passwordHash = $hash;
        $user->status = self::STATUS_ACTIVE;

        return $user;
    }

    public function edit(Email $email, Name $name): void
    {
        $this->email = $email;
        $this->name  = $name;
    }

    public static function signUpByEmail(
        Id $id,
        \DateTimeImmutable $date,
        Name $name,
        Email $email,
        string $hash,
        string $token
    ): self {
      $user = new self($id, $date, $name);
      $user->email = $email;
      $user->passwordHash = $hash;
      $user->confirmToken = $token;
      $user->status = self::STATUS_WAIT;

      return $user;
    }

    public function confirmSignUp(): void
    {
        if (!$this->isWait()) {
            throw new \DomainException('User was already confirmed.');
        }

        $this->status = self::STATUS_ACTIVE;
        $this->confirmToken = null;
    }

    public static function signUpByNetwork(
        Id $id,
        \DateTimeImmutable $date,
        Name $name,
        string $network,
        string $identity
    ): self {
        $user = new self($id, $date, $name);
        $user->attachNetwork($network, $identity);
        $user->status = self::STATUS_ACTIVE;

        return $user;
    }

    public function attachNetwork(string $network, string $identity): void
    {
       foreach ($this->networks as $existing) {
           if ($existing->isForNetwork($network)) {
               throw new  \DomainException('Network was already attached.');
           }
       }

       $this->networks->add(new Network($this, $network, $identity));
    }

    public function detachNetwork(string $network, string $identity): void
    {
        foreach ($this->networks as $existing) {
            if ($existing->isFor($network, $identity)) {
                if ((! $this->email) && $this->networks->count() === 1) {
                    throw new \DomainException('Unable to detach the last user`s identity.');
                }

                $this->networks->removeElement($existing);
                return;
            }
        }

        throw new \DomainException('Network has not been attached.');
    }

    public function requestPasswordReset(
        ResetToken $token,
        \DateTimeImmutable $date
    ): void {
        if (!$this->isActive()) {
            throw new \DomainException('User is not active.');
        }

        if (!$this->email) {
            throw new \DomainException('Email is not specified.');
        }

        if ($this->resetToken && !$this->resetToken->isExpiresTo($date)) {
            throw new \DomainException('Resetting was already requested.');
        }

        $this->resetToken = $token;
    }

    public function passwordReset(
        \DateTimeImmutable $date,
        string $hash
    ): void {
        if (!$this->resetToken) {
            throw new \DomainException('Resetting was not requested.');
        }

        if ($this->resetToken->isExpiresTo($date)) {
            throw new \DomainException('Reset token expired.');
        }

        $this->passwordHash = $hash;
        $this->resetToken = null;
    }

    public function requestEmailChanging(
        Email $email,
        string $token
    ): void {
        if (!$this->isActive()) {
            throw new \DomainException('User is not active.');
        }

        if ($this->email && $this->email->isEqual($email)) {
            throw new \DomainException('Given email is equal previous.');
        }

        $this->newEmail = $email;
        $this->newEmailToken = $token;
    }

    public function confirmEmailChanging(string $token): void
    {
        if (!$this->newEmailToken) {
            throw new \DomainException('Changing was not requested.');
        }

        if ($this->newEmailToken !== $token) {
            throw new \DomainException('Incorrect changing token.');
        }

        $this->email = $this->newEmail;
        $this->newEmail = null;
        $this->newEmailToken = null;
    }

    public function changeName(Name $name): void
    {
        $this->name = $name;
    }

    public function changeRole(Role $newRole): void
    {
        if ($this->role->isEqual($newRole)) {
            throw new \DomainException('Given role equal the previous.');
        }

        $this->role = $newRole;
    }

    public function activate(): void
    {
        if ($this->isActive()) {
            throw new \DomainException('User is already active.');
        }

        $this->status = self::STATUS_ACTIVE;
    }

    public function block(): void
    {
        if ($this->isBlocked()) {
            throw new \DomainException('User is already blocked.');
        }

        $this->status = self::STATUS_BLOCKED;
    }

    /**
     * Check the user's status
     *
     * @return  bool
     */
    public function isWait(): bool
    {
        return $this->status === self::STATUS_WAIT;
    }

    /**
     * Check the user's active status
     *
     * @return  bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * Check the user's blocked status
     *
     * @return  bool
     */
    public function isBlocked(): bool
    {
        return $this->status === self::STATUS_BLOCKED;
    }

    /**
     * Get the value of id object
     *
     * @return  Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Get the value of date
     *
     * @return  \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * Get the value of email object
     *
     * @return  Email
     */
    public function getEmail(): ?Email
    {
        return $this->email;
    }

    /**
     * Get the value of passwordHash
     *
     * @return  string
     */
    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    /**
     * Get the value of confirmToken
     *
     * @return  string|null
     */
    public function getConfirmToken(): ?string
    {
        return $this->confirmToken;
    }

    /**
     * Get the value of name
     *
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * Get the value of status
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Get Networks
     *
     * @return Network[]
     */
    public function getNetworks(): array
    {
        return $this->networks->toArray();
    }

    /**
     * Get the value of resetToken
     *
     * @return  ResetToken|null
     */
    public function getResetToken(): ?ResetToken
    {
        return $this->resetToken;
    }

    /**
     * Get the value of role
     *
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * Get the value of newEmail
     *
     * @return Email
     */
    public function getNewEmail(): ?Email
    {
        return $this->newEmail;
    }

    /**
     * Get the value of newEmailToken
     *
     * @return string
     */
    public function getNewEmailToken(): ?string
    {
        return $this->newEmailToken;
    }

    /**
     * @ORM\PostLoad()
     */
    public function checkEmbeds(): void
    {
        if ($this->resetToken->isEmpty()) {
            $this->resetToken = null;
        }
    }
}
