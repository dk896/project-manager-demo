<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Role;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="work_projects_roles")
 */
class Role
{
    /**
     * @var Id
     * @ORM\Column(type="work_projects_role_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @var ArrayCollection|Permission[]
     * @ORM\Column(type="work_projects_role_permissions")
     */
    private $permissions;

    /**
     * @ORM\Version()
     * @ORM\Column(type="integer")
     */
    private $version;

    /**
     * Role constructor
     *
     * @param Id $id
     * @param string $name
     * @param string[] $permissions
     */
    public function __construct(
        Id $id,
        string $name,
        array $permissions
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->setPermissions($permissions);
    }

    /**
     * @param string $name
     * @param string[] $permissions
     * @return void
     */
    public function edit(string $name, array $permissions): void
    {
        $this->name = $name;
        $this->setPermissions($permissions);
    }

    /**
     * @param string $permission
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        return $this->permissions->exists(
            static function ($key, Permission $current) use ($permission) {
                return $current->isNameEqual($permission);
            }
        );
    }

    /**
     * @param Id $id
     * @param string $name
     * @return self
     */
    public function clone(Id $id, string $name): self
    {
        return new self(
            $id,
            $name,
            array_map(
                static function(Permission $permission) {
                    return $permission->getName();
                },
                $this->permissions->toArray()
            )
        );
    }

    /**
     * @return Id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->permissions->toArray();
    }

    /**
     * @param string[] $names
     * @return void
     */
    public function setPermissions(array $names): void
    {
        $this->permissions = new ArrayCollection(
            array_map(static function (string $name) {
                return new Permission($name);
            }, array_unique($names))
        );
    }
}
