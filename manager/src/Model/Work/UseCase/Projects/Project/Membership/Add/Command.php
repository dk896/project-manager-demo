<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Project\Membership\Add;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     * @property string $project - value of project ID
     */
    public $project;

    /**
     * @Assert\NotBlank()
     * @property string $member - value of member ID
     */
    public $member;

    /**
     * @Assert\NotBlank()
     * @property array $departments
     * array of values departments IDs
     */
    public $departments;

    /**
     * @Assert\NotBlank()
     * @property array $roles
     * array of values departments IDs
     */
    public $roles;

    public function __construct(string $project)
    {
        $this->project = $project;
    }
}
