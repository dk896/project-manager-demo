<?php

declare(strict_types=1);

namespace App\Model\User\Entity\User;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class ResetToken
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $expires;

    public function __construct(
        string $token,
        \DateTimeImmutable $expires
    ) {
        $this->token = $token;
        $this->expires = $expires;
    }

    public function isExpiresTo(\DateTimeImmutable $date): bool
    {
        return $this->expires <= $date;
    }

    /**
     * Get the value of token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @internal for postLoad callback
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return empty($this->token);
    }
}
