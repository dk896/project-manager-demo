<?php

declare(strict_types=1);

namespace App\Controller\Profile\OAuth;

use App\Controller\ErrorHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\User\UseCase\Network\Detach\Command;
use App\Model\User\UseCase\Network\Detach\Handler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/profile/oauth")
 */
class DetachController extends AbstractController
{
    private $errors;

    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route(
     *  "/detach/{network}/{identity}",
     *  name="profile.oauth.detach",
     *  methods={"DELETE"}
     * )
     * @param Request $request
     * @param string $network
     * @param string $identity
     * @param Handler $handler
     * @return Response
     */
    public function detach(
        Request $request,
        string $network,
        string $identity,
        Handler $handler
    ): Response {
        if (
            ! $this->isCsrfTokenValid(
                'delete',
                $request->request->get('token')
            )
        ) {
            return $this->redirectToRoute('/profile');
        }

        $command = new Command(
            $this->getUser()->getId(),
            $network,
            $identity
        );

        try {
            $handler->handle($command);

            return $this->redirectToRoute('/profile');
        } catch (\DomainException $err) {
            $this->errors->handle($err);
            $this->addFlash('error', $err->getMessage());

            return $this->redirectToRoute('/profile');
        }
    }
}
