<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Task;

use App\Model\EventsTrait;
use App\Model\AggregateRoot;
use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;
use App\Model\Work\Entity\Projects\Task\Event;
use App\Model\Work\Entity\Members\Member\Member;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\Work\Entity\Projects\Task\File\File;
use App\Model\Work\Entity\Projects\Task\File\Info;
use App\Model\Work\Entity\Projects\Project\Project;
use App\Model\Work\Entity\Projects\Task\Change\Set;
use App\Model\Work\Entity\Projects\Task\Change\Change;
use App\Model\Work\Entity\Members\Member\Id as MemberId;
use App\Model\Work\Entity\Projects\Task\File\Id as FileId;
use App\Model\Work\Entity\Projects\Task\Change\Id as ChangeId;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *  name="work_projects_tasks",
 *  indexes={
 *      @ORM\Index(columns={"date"})
 *  })
 */
class Task implements AggregateRoot
{
    use EventsTrait;

    /**
     * @var Id
     * @ORM\Column(type="work_projects_task_id")
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="work_projects_tasks_seq", initialValue=1)
     * @ORM\Id
     */
    private $id;
    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="App\Model\Work\Entity\Projects\Project\Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     */
    private $project;
    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity="App\Model\Work\Entity\Members\Member\Member")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=false)
     */
    private $author;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;
    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private $planDate;
    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private $startDate;
    /**
     * @var \DateTimeImmutable|null
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private $endDate;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *  targetEntity="App\Model\Work\Entity\Projects\Task\File\File",
     *  mappedBy="task",
     *  orphanRemoval=true,
     *  cascade={"all"}
     * )
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $files;
    /**
     * @var Type
     * @ORM\Column(type="work_projects_task_type", length=16)
     */
    private $type;
    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $progress;
    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $priority;
    /**
     * @var Task|null
     * @ORM\ManyToOne(targetEntity="Task")
     * @ORM\JoinColumn(
     *  name="parent_id",
     *  referencedColumnName="id",
     *  nullable=true,
     *  onDelete="SET NULL"
     * )
     */
    private $parent;
    /**
     * @var Status
     * @ORM\Column(type="work_projects_task_status", length=16)
     */
    private $status;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Work\Entity\Members\Member\Member")
     * @ORM\JoinTable(
     *  name="work_projects_tasks_executors",
     *  joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="member_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name.first" = "ASC"})
     */
    private $executors;
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *  targetEntity="App\Model\Work\Entity\Projects\Task\Change\Change",
     *  mappedBy="task",
     *  orphanRemoval=true,
     *  cascade={"persist"}
     * )
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $changes;

    /**
     * @ORM\Version()
     * @ORM\Column(type="integer")
     */
    private $version;

    public function __construct(
        Id $id,
        Project $project,
        Member $author,
        \DateTimeImmutable $date,
        string $name,
        ?string $content,
        Type $type,
        int $priority
    ) {
        $this->id = $id;
        $this->project = $project;
        $this->author = $author;
        $this->date = $date;
        $this->name = $name;
        $this->content = $content;
        $this->files = new ArrayCollection();
        $this->type = $type;
        $this->progress = 0;
        $this->priority = $priority;
        $this->status = Status::snew();
        $this->executors = new ArrayCollection();
        $this->changes = new ArrayCollection();
        $this->addChange(
            $author,
            $date,
            Set::forNewTask(
                $project->getId(),
                $name,
                $content,
                $type,
                $priority
            )
        );
    }

    public function addFile(
        Member $actor,
        \DateTimeImmutable $date,
        FileId $id,
        Info $info
    ): void {
        $this->files->add(new File($this, $id, $actor, $date, $info));
        $this->addChange($actor, $date, Set::fromFile($id));

        $this->recordEvent(
            new Event\TaskFileAdded($actor->getId(), $this->id, $id, $info)
        );
    }

    public function removeFile(
        Member $actor,
        \DateTimeImmutable $date,
        FileId $id
    ): void {
        foreach ($this->files as $current) {
            if ($current->getId()->isEqual($id)) {
                $this->files->removeElement($current);

                $this->addChange(
                    $actor,
                    $date,
                    Set::fromRemovedFile($current->getId())
                );

                $this->recordEvent(
                    new Event\TaskFileRemoved(
                        $actor->getId(),
                        $this->id,
                        $id,
                        $current->getInfo()
                    )
                );

                return;
            }
        }

        throw new \DomainException('File is not found.');
    }

    public function assignExecutor(
        Member $actor,
        \DateTimeImmutable $date,
        Member $executor
    ): void {
        if ($this->executors->contains($executor)) {
            throw new \DomainException('Given executor is already assigned.');
        }

        $this->executors->add($executor);

        $this->addChange(
            $actor,
            $date,
            Set::fromExecutor($executor->getId())
        );

        $this->recordEvent(
            new Event\TaskExecutorAssigned(
                $actor->getId(),
                $this->id,
                $executor->getId()
            )
        );
    }

    public function revokeExecutor(
        Member $actor,
        \DateTimeImmutable $date,
        MemberId $id
    ): void {
        foreach ($this->executors as $current) {
            if ($current->getId()->isEqual($id)) {
                $this->executors->removeElement($current);

                $this->addChange(
                    $actor,
                    $date,
                    Set::fromRevokedExecutor($current->getId())
                );

                $this->recordEvent(
                    new Event\TaskExecutorRevoked(
                        $actor->getId(),
                        $this->id,
                        $current->getId()
                    )
                );

                return;
            }
        }

        throw new \DomainException('Given executor is not assigned.');
    }

    public function hasExecutor(MemberId $id): bool
    {
        foreach ($this->executors as $executor) {
            if ($executor->getId()->isEqual($id)) {
                return true;
            }
        }

        return false;
    }

    public function start(Member $actor, \DateTimeImmutable $date): void
    {
        if (!$this->isNew()) {
            throw new \DomainException('Task is already started.');
        }

        if (!$this->executors->count()) {
            throw new \DomainException('Task has not any executor.');
        }

        $this->changeStatus($actor, $date, Status::working());
    }

    public function plan(
        Member $actor,
        \DateTimeImmutable $date,
        \DateTimeImmutable $planDate
    ): void {
        $this->planDate = $planDate;
        $this->addChange($actor, $date, Set::fromPlan($planDate));
        $this->recordEvent(
            new Event\TaskPlanChanged($actor->getId(), $this->id, $this->planDate)
        );
    }

    public function removePlan(Member $actor, \DateTimeImmutable $date): void
    {
        $this->planDate = null;
        $this->addChange($actor, $date, Set::forRemovedPlan());
        $this->recordEvent(
            new Event\TaskPlanChanged($actor->getId(), $this->id, null)
        );
    }

    public function edit(
        Member $actor,
        \DateTimeImmutable $date,
        string $name,
        ?string $content
    ): void {
        if ($name !== $this->name) {
            $this->name = $name;
            $this->addChange($actor, $date, Set::fromName($name));
        }

        if ($content !== $this->content && !empty($content)) {
            $this->content = $content;
            $this->addChange($actor, $date, Set::fromContent($content));
        }

        $this->recordEvent(
            new Event\TaskEdited(
                $actor->getId(),
                $this->id,
                $this->name,
                $this->content
            )
        );
    }

    public function setRoot(Member $actor, \DateTimeImmutable $date): void
    {
        $this->parent = null;
        $this->addChange($actor, $date, Set::forRemovedParent());
    }

    public function setChildOf(
        Member $actor,
        \DateTimeImmutable $date,
        Task $parent
    ): void {
        if ($parent === $this->parent) {
            return;
        }

        $current = $parent;

        do {
            if ($current === $this) {
                throw new \DomainException('Cannot set cyclomatic child.');
            }
        }
        while ($current && $current = $current->getParent());

        $this->parent = $parent;
        $this->addChange($actor, $date, Set::fromParent($parent->getId()));
    }

    public function move(
        Member $actor,
        \DateTimeImmutable $date,
        Project $destination
    ): void {
        if ($destination === $this->project) {
            throw new \DomainException('Project is the same as destination.');
        }

        $this->project = $destination;
        $this->addChange($actor, $date, Set::fromProject($destination->getId()));
    }

    public function changeType(
        Member $actor,
        \DateTimeImmutable $date,
        Type $newType
    ): void {
        if ($this->type->isEqual($newType)) {
            throw new \DomainException('Given type is the same as previous.');
        }

        $this->type = $newType;
        $this->addChange($actor, $date, Set::fromType($newType));
        $this->recordEvent(
            new Event\TaskTypeChanged($actor->getId(), $this->id, $this->type)
        );
    }

    public function changeStatus(
        Member $actor,
        \DateTimeImmutable $date,
        Status $newStatus
    ): void {
        if ($this->status->isEqual($newStatus)) {
            throw new \DomainException('Given status is the same as previous.');
        }

        $this->status = $newStatus;
        $this->addChange($actor, $date, Set::fromStatus($newStatus));
        $this->recordEvent(
            new Event\TaskStatusChanged($actor->getId(), $this->id, $this->status)
        );

        if (!$this->isNew() && !$this->startDate) {
            $this->startDate = $date;
        }

        if (!$this->status->isDone()) {
            $this->endDate = null;

            return;
        }

        if ($this->status->isDone()) {
            if ($this->progress !== 100){
                $this->changeProgress($actor, $date, 100);
            }

            $this->endDate = $date;
        }
    }

    public function isNew(): bool
    {
        return $this->status->isNew();
    }

    public function isWorking(): bool
    {
        return $this->status->isWorking();
    }

    public function changeProgress(
        Member $actor,
        \DateTimeImmutable $date,
        int $newProgress
    ): void {
        Assert::range($newProgress, 0, 100);

        if ($newProgress === $this->progress) {
            throw new \DomainException('Given progress is the same as previous.');
        }

        $this->progress = $newProgress;
        $this->addChange($actor, $date, Set::fromProgress($newProgress));
        $this->recordEvent(
            new Event\TaskProgressChanged($actor->getId(), $this->id, $this->progress)
        );
    }

    public function changePriority(
        Member $actor,
        \DateTimeImmutable $date,
        int $newPriority
    ): void {
        Assert::range($newPriority, 1, 4);

        if ($newPriority === $this->priority) {
            throw new \DomainException('Given priority is the same as previous.');
        }

        $this->priority = $newPriority;
        $this->addChange($actor, $date, Set::fromPriority($newPriority));
        $this->recordEvent(
            new Event\TaskPriorityChanged($actor->getId(), $this->id, $this->priority)
        );
    }

    /**
     * Get the value of id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Get the value of project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * Get the value of author
     */
    public function getAuthor(): Member
    {
        return $this->author;
    }

    /**
     * Get the value of date
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the value of content
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Get the value of type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * Get the value of progress
     */
    public function getProgress(): int
    {
        return $this->progress;
    }

    /**
     * Get the value of priority
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * Get the value of planDate
     */
    public function getPlanDate(): ?\DateTimeImmutable
    {
        return $this->planDate;
    }

    /**
     * Get the value of parent
     */
    public function getParent(): ?Task
    {
        return $this->parent;
    }

    public function getStatus(): Status
    {
       return $this->status;
    }

    /** @return Member[] */
    public function getExecutors(): array
    {
       return $this->executors->toArray();
    }

    /** @return ?\DateTimeImmutable */
    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->startDate;
    }

    /** @return ?\DateTimeImmutable */
    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    /** @return File[] */
    public function getFiles(): array
    {
        return $this->files->toArray();
    }

    /** @return Change[] */
    public function getChanges(): array
    {
        return $this->changes->toArray();
    }

    private function addChange(Member $actor, \DateTimeImmutable $date, Set $set): void
    {
        /** @var Change $last */
        if ($last = $this->changes->last()) {
            /** @var ChangeId $next */
            $next = $last->getId()->next();
        }

        if (!$last) {
            /** @var ChangeId $next */
            $next = ChangeId::first();
        }

        $this->changes->add(new Change($this, $next, $actor, $date, $set));
    }
}
