<?php

declare(strict_types=1);

namespace App\Event\Listener\Work\Projects\Task;

use Twig\Environment;
use App\Model\Work\Entity\Projects\Task\TaskRepository;
use App\Model\Work\Entity\Members\Member\MemberRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Model\Work\Entity\Projects\Task\Event\TaskExecutorAssigned;

class EmailNotificationSubscriber implements EventSubscriberInterface
{
    private $tasks;
    private $members;
    private $mailer;
    private $twig;

    public function __construct(
        TaskRepository $tasks,
        MemberRepository $members,
        \Swift_Mailer $mailer,
        Environment $twig
    ) {
        $this->tasks = $tasks;
        $this->members = $members;
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            TaskExecutorAssigned::class => [
                ['onTaskExecutorAssignedExecutor'],
                ['onTaskExecutorAssignedAuthor'],
            ]
        ];
    }

    public function onTaskExecutorAssignedExecutor(TaskExecutorAssigned $event): void
    {
        if ($event->executorId->isEqual($event->actorId)) {
            return;
        }

        $task = $this->tasks->get($event->taskId);
        $author = $task->getAuthor();
        $executor = $this->members->get($event->executorId);

        if ($executor === $author) {
            return;
        }

        $message = (new \Swift_Message('You were assigned to the task'))
            ->setTo([
                $executor->getEmail()->getValue() => $executor->getName()->getFull()
            ])
            ->setBody(
                $this->twig->render(
                    'mail/work/projects/task/executor-assigned-executor.html.twig',
                    [
                        'task' => $task,
                        'executor' => $executor,
                    ]
                ),
                'text/html'
            )
        ;

        if (!$this->mailer->send($message)) {
            throw new \RuntimeException('Unable to send message.');
        }
    }

    public function onTaskExecutorAssignedAuthor(TaskExecutorAssigned $event): void
    {
        $task = $this->tasks->get($event->taskId);
        $author = $task->getAuthor();
        $executor = $this->members->get($event->executorId);

        if ($executor === $author) {
            return;
        }

        $message = (new \Swift_Message('There was assigned executor for your task'))
            ->setTo([
                $author->getEmail()->getValue() => $author->getName()->getFull()
            ])
            ->setBody(
                $this->twig->render(
                    'mail/work/projects/task/executor-assigned-author.html.twig',
                    [
                        'task' => $task,
                        'author' => $author,
                        'executor' => $executor,
                    ]
                ),
                'text/html'
            )
        ;

        if (!$this->mailer->send($message)) {
            throw new \RuntimeException('Unable to send message.');
        }
    }
}
