<?php

declare(strict_types=1);

namespace App\Tests\Builder\Work\Projects;

use App\Model\Work\Entity\Projects\Task\Id as TaskId;
use App\Model\Work\Entity\Projects\Project\Project;
use App\Model\Work\Entity\Projects\Task\Type;
use App\Model\Work\Entity\Projects\Task\Task;
use App\Model\Work\Entity\Members\Member\Member;

class TaskBuilder
{
    private $id;
    private $date;
    private $name;
    private $content;
    private $type;
    private $priority;

    public function __construct()
    {
        $this->id = new TaskId(1);
        $this->date = new \DateTimeImmutable();
        $this->name = 'Hard task';
        $this->content = 'Wise content';
        $this->type = new Type(Type::FEATURE);
        $this->priority = 1;
    }

    public function withId(TaskId $id): self
    {
        $clone = clone $this;
        $clone->id = $id;

        return $clone;
    }

    public function withType(Type $newType): self
    {
        $clone = clone $this;
        $clone->type = $newType;

        return $clone;
    }

    public function build(Project $project, Member $member): Task
    {
        return new Task(
            $this->id,
            $project,
            $member,
            $this->date,
            $this->name,
            $this->content,
            $this->type,
            $this->priority
        );
    }
}
