<?php

declare(strict_types=1);

namespace App\Widget\Work\Projects\MyTasks;

use Twig\Environment;
use Twig\TwigFunction;
use App\Security\UserIdentity;
use Twig\Extension\AbstractExtension;
use App\ReadModel\Work\Projects\Task\TaskFetcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MyTasksWidget extends AbstractExtension
{
    private const LIMIT = 10;

    /**
     * @var TaskFetcher
     */
    private $tasks;
    /**
     * @var TokenStorageInterface
     */
    private $tokens;

    public function __construct(
        TaskFetcher $tasks,
        TokenStorageInterface $tokens
    ) {
        $this->tasks = $tasks;
        $this->tokens = $tokens;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'work_projects_my_tasks',
                [$this, 'tasks'],
                [
                    'needs_environment' => true,
                    'is_safe' => ['html'],
                ]
            )
        ];
    }

    public function tasks(Environment $twig): string
    {
        $token = $this->tokens->getToken();

        if (null === $token) {
            return '';
        }

        if (! ($user = $token->getUser()) instanceof UserIdentity) {
            return '';
        }

        $results = $this->tasks->lastForMe($user->getId(), self::LIMIT);

        return $twig
            ->render(
                'widget/work/projects/my_tasks/my_tasks.html.twig',
                [
                    'tasks' => $results,
                ]
            );
    }
}
