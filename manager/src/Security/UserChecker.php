<?php

declare(strict_types=1);

namespace App\Security;

use App\Security\UserIdentity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\Exception\DisabledException;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $identity): void
    {
        if (! $identity instanceof UserIdentity) {
            return;
        }

        if (!$identity->isActive()) {
            $exception = new DisabledException('User`s account is disabled.');
            $exception->setUser($identity);

            throw $exception;
        }
    }

    public function checkPostAuth(UserInterface $identity): void
    {
        if (! $identity instanceof UserIdentity) {
            return;
        }
    }
}
