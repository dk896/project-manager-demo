<?php

namespace App\DataFixtures;

use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\Name;
use App\Model\User\Entity\User\Role;
use App\Model\User\Entity\User\User;
use App\Model\User\Entity\User\Email;
use App\Model\User\Service\PasswordHasher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class UserFixture extends Fixture
{
    public const REFERENCE_ADMIN = 'user_user_admin';
    public const REFERENCE_USER = 'user_user_user';

    private $hasher;

    /** @var Generator $faker */
    private $faker;

    public function __construct(PasswordHasher $hasher)
    {
        $this->hasher = $hasher;
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $hash = $this->hasher->hash('password');

        $network = $this->createSignedUpByNetwork(
            new Name($this->faker->firstName, $this->faker->lastName),
            'facebook',
            '10000'
        );

        $manager->persist($network);

        $requested = $this->createSignUpRequestedByEmail(
            new Name($this->faker->firstName, $this->faker->lastName),
            new Email($this->faker->email),
            $hash
        );

        $manager->persist($requested);

        $confirmed = $this->createSignUpConfirmedByEmail(
            new Name($this->faker->firstName, $this->faker->lastName),
            new Email($this->faker->email),
            $hash
        );

        $manager->persist($confirmed);
        $this->setReference(self::REFERENCE_USER, $confirmed);

        $known = $this->createSignUpConfirmedByEmail(
            new Name('Alex', 'Bowlduin'),
            new Email('alex.bowlduin@app.org'),
            $hash
        );

        $manager->persist($known);

        $admin = $this->createAdminByEmail(
            new Name($this->faker->firstName, $this->faker->lastName),
            new Email('admin@app.org'),
            $hash
        );

        $manager->persist($admin);
        $this->setReference(self::REFERENCE_ADMIN, $admin);

        $manager->flush();
    }

    private function createAdminByEmail(
        Name $name,
        Email $email,
        string $hash
    ): User {
        $user = $this->createSignUpConfirmedByEmail(
            $name,
            $email,
            $hash
        );

        $user->changeRole(Role::admin());

        return $user;
    }

    private function createSignUpConfirmedByEmail(
        Name $name,
        Email $email,
        string $hash
    ): User {
        $user = $this->createSignUpRequestedByEmail(
            $name,
            $email,
            $hash
        );

        $user->confirmSignUp();

        return $user;
    }

    private function createSignUpRequestedByEmail(
        Name $name,
        Email $email,
        string $hash
    ): User {
        return User::signUpByEmail(
            Id::next(),
            new \DateTimeImmutable(),
            $name,
            $email,
            $hash,
            'token'
        );
    }

    private function createSignedUpByNetwork(
        Name $name,
        string $network,
        string $identity
    ): User {
       return User::signUpByNetwork(
         Id::next(),
         new \DateTimeImmutable(),
         $name,
         $network,
         $identity
       );
    }
}
