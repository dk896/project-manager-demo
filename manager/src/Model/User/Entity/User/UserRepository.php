<?php

declare(strict_types=1);

namespace App\Model\User\Entity\User;

use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\User;
use App\Model\User\Entity\User\Email;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\EntityNotFoundException;

class UserRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(User::class);
    }

    /**
     * @param Id $id
     * @return User
     */
    public function get(Id $id): User
    {
        /** @var User $user */
        if (!$user = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('User is not found.');
        }

        return $user;
    }

    /**
     * @param Email $email
     * @return User
     */
    public function getByEmail(Email $email): User
    {
        /** @var User $user */
        if (!$user = $this->repo->findOneBy([
            'email' => $email->getValue()
        ])) {
            throw new EntityNotFoundException('User is not found.');
        }

        return $user;
    }

    /**
     * @param Email $email
     * @return boolean
     */
    public function hasByEmail(Email $email): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->andWhere('t.email = :email')
            ->setParameter(':email', $email->getValue())
            ->getQuery()
            ->getSingleScalarResult()
                > 0;
    }

    /**
     * @param string $token
     * @return User|null
     */
    public function findByConfirmToken(string $token): ?User
    {
        return $this->repo->findOneBy([
            'confirmToken' => $token
        ]);
    }

    /**
     * @param string $token
     * @return User|null
     */
    public function findByResetToken(string $token): ?User
    {
        return $this->repo->findOneBy([
            'resetToken.token' => $token
        ]);
    }

    /**
     * @param string $network
     * @param string $identity
     * @return boolean
     */
    public function hasByNetworkIdentity(string $network, string $identity): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->innerJoin('t.networks', 'n')
            ->andWhere('n.network = :network AND n.identity = :identity')
            ->setParameter(':network', $network)
            ->setParameter(':identity', $identity)
            ->getQuery()
            ->getSingleScalarResult()
                > 0;
    }

    /**
     * @param User $user
     * @return void
     */
    public function add(User $user): void
    {
        $this->em->persist($user);
    }
}
