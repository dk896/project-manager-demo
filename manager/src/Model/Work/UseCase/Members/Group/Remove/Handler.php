<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Members\Group\Remove;

use App\Model\Flusher;
use App\Model\Work\Entity\Members\Group\Id;
use Symfony\Component\Validator\Constraints as Assert;
use App\Model\Work\Entity\Members\Group\GroupRepository;
use App\Model\Work\Entity\Members\Member\MemberRepository;

class Handler
{
    private $groups;
    private $flusher;
    private $members;

    public function __construct(
        GroupRepository $groups,
        MemberRepository $members,
        Flusher $flusher
    ) {
        $this->groups = $groups;
        $this->members = $members;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $group = $this->groups->get(new Id($command->id));

        if ($this->members->hasByGroup($group->getId())) {
            throw new \DomainException('Cannot remove group with members.');
        }

        $this->groups->remove($group);

        $this->flusher->flush();
    }
}
