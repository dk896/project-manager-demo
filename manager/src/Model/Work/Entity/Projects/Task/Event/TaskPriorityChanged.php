<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Task\Event;

use App\Model\Work\Entity\Projects\Task\Id as TaskId;
use App\Model\Work\Entity\Members\Member\Id as MemberId;

class TaskPriorityChanged
{
    public $actorId;
    public $taskId;
    public $priority;

    public function __construct(
        MemberId $actorId,
        TaskId $taskId,
        int $priority
    ) {
        $this->actorId = $actorId;
        $this->taskId = $taskId;
        $this->priority = $priority;
    }
}
