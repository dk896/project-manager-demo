<?php

namespace App\Tests\Functional\OAuth;

use App\Tests\Functional\DbWebTestCase;
use App\Tests\Functional\OAuth\OAuthFixture;

class PasswordTest extends DbWebTestCase
{
    private const URI = '/token';

    public function testMethod(): void
    {
        $this->client->request('GET', self::URI);
        // 405 Method Not Allowed
        self::assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    public function testSuccess(): void
    {
        $this->client->request(
            'POST',
            self::URI,
            [
                'grant_type' => 'password',
                'username' => OAuthFixture::OAUTH_EMAIL,
                'password' => OAuthFixture::OAUTH_PASSWORD,
                'client_id' => OAuthFixture::CLIENT_ID,
                'client_secret' => OAuthFixture::CLIENT_SECRET,
                'access_type' => 'offline',
            ]
        );

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());

        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'token_type' => 'Bearer',
        ], $data);

        self::assertArrayHasKey('expires_in', $data);
        self::assertNotEmpty($data['expires_in']);

        self::assertArrayHasKey('access_token', $data);
        self::assertNotEmpty($data['access_token']);

        self::assertArrayHasKey('refresh_token', $data);
        self::assertNotEmpty($data['refresh_token']);
    }

    public function testInvalid(): void
    {
        $this->client->request(
            'POST',
            self::URI,
            [
                'grant_type' => 'password',
                'username' => OAuthFixture::OAUTH_EMAIL,
                'password' => 'invalid',
                'client_id' => OAuthFixture::CLIENT_ID,
                'client_secret' => OAuthFixture::CLIENT_SECRET,
            ]
        );

        // 401 Unauthorized
        self::assertEquals(401, $this->client->getResponse()->getStatusCode());
    }
}
