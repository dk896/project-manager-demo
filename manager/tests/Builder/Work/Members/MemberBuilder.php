<?php

declare(strict_types=1);

namespace App\Tests\Builder\Work\Members;

use App\Model\Work\Entity\Members\Group\Group;
use App\Model\Work\Entity\Members\Member\Name;
use App\Model\Work\Entity\Members\Member\Email;
use App\Model\Work\Entity\Members\Member\Id as MemberId;
use App\Model\Work\Entity\Members\Member\Member;

class MemberBuilder
{
    private $id;
    private $name;
    private $email;

    public function __construct()
    {
        $this->id = MemberId::next();
        $this->name = new Name('Jorge', 'Wikerson');
        $this->email = new Email('j.wikerson@app.org');
    }

    public function withId(MemberId $id): self
    {
        $clone = clone $this;
        $clone->id = $id;

        return $clone;
    }

    public function withEmail(Email $email): self
    {
        $clone = clone $this;
        $clone->email = $email;

        return $clone;
    }

    public function build(Group $group): Member
    {
        return new Member(
            $this->id,
            $group,
            $this->name,
            $this->email
        );
    }
}
