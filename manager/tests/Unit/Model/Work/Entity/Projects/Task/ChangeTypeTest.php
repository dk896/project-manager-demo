<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\Work\Entity\Projects\Task;

use PHPUnit\Framework\TestCase;
use App\Model\Work\Entity\Projects\Task\Type;
use App\Tests\Builder\Work\Members\GroupBuilder;
use App\Tests\Builder\Work\Projects\TaskBuilder;
use App\Tests\Builder\Work\Members\MemberBuilder;
use App\Tests\Builder\Work\Projects\ProjectBuilder;

class ChangeTypeTest extends TestCase
{
    public function testSuccess(): void
    {
        $group = (new GroupBuilder())->build();
        $member = (new MemberBuilder())->build($group);
        $project = (new ProjectBuilder())->build();
        $task = (new TaskBuilder())
            ->withType(new Type(Type::NONE))
            ->build($project, $member);

        $task->changeType(
            $member,
            new \DateTimeImmutable(),
            $type = new Type(Type::FEATURE)
        );

        self::assertEquals($type, $task->getType());
    }

    public function testAlready(): void
    {
        $group = (new GroupBuilder())->build();
        $member = (new MemberBuilder())->build($group);
        $project = (new ProjectBuilder())->build();

        $task = (new TaskBuilder())
            ->withType($type = new Type(Type::FEATURE))
            ->build($project, $member);

        $this->expectExceptionMessage('Given type is the same as previous.');
        $task->changeType(
            $member,
            new \DateTimeImmutable(),
            $type
        );
    }
}
