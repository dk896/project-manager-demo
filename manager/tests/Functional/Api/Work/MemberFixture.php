<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api\Work;

use App\Model\User\Entity\User\User;
use App\Tests\Functional\AuthFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Tests\Builder\Work\Members\GroupBuilder;
use App\Tests\Builder\Work\Members\MemberBuilder;
use App\Model\Work\Entity\Members\Member\Id as MemberId;
use App\Model\Work\Entity\Members\Member\Email as MemberEmail;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MemberFixture extends Fixture implements DependentFixtureInterface
{
    public const REFERENCE_MEMBER_ADMIN = 'test_work_member_admin';
    public const REFERENCE_MEMBER_USER = 'test_work_member_user';

    public function load(ObjectManager $manager): void
    {
        $group = (new GroupBuilder())
            ->withName('Our clients')
            ->build();

        $manager->persist($group);

        $user = $this->getUser(AuthFixture::REFERENCE_USER);

        $member = (new MemberBuilder())
            ->withId(new MemberId($user->getId()->getValue()))
            ->withEmail(new MemberEmail($user->getEmail()->getValue()))
            ->build($group);

        $manager->persist($member);
        $this->setReference(self::REFERENCE_MEMBER_USER, $member);

        $group = (new GroupBuilder())
            ->withName('Our staff')
            ->build();

        $manager->persist($group);

        $admin = $this->getUser(AuthFixture::REFERENCE_ADMIN);

        $member = (new MemberBuilder())
            ->withId(new MemberId($admin->getId()->getValue()))
            ->withEmail(new MemberEmail($admin->getEmail()->getValue()))
            ->build($group);

        $manager->persist($member);
        $this->setReference(self::REFERENCE_MEMBER_ADMIN, $member);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            AuthFixture::class,
        ];
    }

    private function getUser(string $refernce): User
    {
        return $this->getReference($refernce);
    }
}
