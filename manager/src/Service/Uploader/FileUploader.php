<?php

namespace App\Service\Uploader;

use Ramsey\Uuid\Uuid;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $storage;
    private $baseUrl;

    public function __construct(
        FilesystemInterface $storage,
        string $baseUrl
    ) {
        $this->storage = $storage;
        $this->baseUrl = $baseUrl;
    }

    public function upload(UploadedFile $file): File
    {
        $path = date('Y/m/d');
        $name = Uuid::uuid4()->toString()
            . '.'
            . $file->getClientOriginalExtension()
        ;

        $this->storage->createDir($path);
        $stream = fopen($file->getRealPath(), 'rb+');
        $this->storage->writeStream($path . '/' . $name, $stream);
        fclose($stream);

        return new File($path, $name, $file->getSize());
    }

    public function generateUrl(string $path): string
    {
        return $this->baseUrl . '/' . $path;
    }

    public function remove(string $path, string $name): void
    {
        $this->storage->delete($path . '/' . $name);
    }
}
