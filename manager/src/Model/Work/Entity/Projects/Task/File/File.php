<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Task\File;

use Doctrine\ORM\Mapping as ORM;
use App\Model\Work\Entity\Members\Member\Member;
use App\Model\Work\Entity\Projects\Task\Task;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *  name="work_projects_task_files",
 *  indexes={
 *      @ORM\Index(columns={"date"})
 * })
 */
class File
{
    /**
     * @var Id
     * @ORM\Column(type="work_projects_task_file_id")
     * @ORM\Id
     */
    private $id;
    /**
     * @var Task
     * @ORM\ManyToOne(targetEntity="App\Model\Work\Entity\Projects\Task\Task", inversedBy="files")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", nullable=false)
     */
    private $task;
    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity="App\Model\Work\Entity\Members\Member\Member")
     * @ORM\JoinColumn(name="member_id", referencedColumnName="id", nullable=false)
     */
    private $member;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;
    /**
     * @var Info
     * @ORM\Embedded(class="Info")
     */
    private $info;

    public function __construct(
        Task $task,
        Id $id,
        Member $member,
        \DateTimeImmutable $date,
        Info $info
    ) {
        $this->task = $task;
        $this->id = $id;
        $this->member = $member;
        $this->date = $date;
        $this->info = $info;
    }

    /**
     * Get the value of id
     *
     * @return  Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Get the value of member
     *
     * @return  Member
     */
    public function getMember(): Member
    {
        return $this->member;
    }

    /**
     * Get the value of info
     *
     * @return  Info
     */
    public function getInfo(): Info
    {
        return $this->info;
    }

    /**
     * Get the value of date
     *
     * @return  \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }
}
