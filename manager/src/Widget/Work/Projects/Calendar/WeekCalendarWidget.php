<?php

declare(strict_types=1);

namespace App\Widget\Work\Projects\Calendar;

use Twig\Environment;
use Twig\TwigFunction;
use App\Security\UserIdentity;
use Twig\Extension\AbstractExtension;
use App\ReadModel\Work\Projects\Calendar\CalendarFetcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class WeekCalendarWidget extends AbstractExtension
{
    /**
     * @var CalendarFetcher
     */
    private $calendar;
    /**
     * @var TokenStorageInterface
     */
    private $tokens;

    public function __construct(
        CalendarFetcher $calendar,
        TokenStorageInterface $tokens
    ) {
        $this->calendar = $calendar;
        $this->tokens = $tokens;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'work_projects_w_calendar',
                [$this, 'calendar'],
                [
                    'needs_environment' => true,
                    'is_safe' => ['html'],
                ]
            )
        ];
    }

    public function calendar(Environment $twig): string
    {
        $token = $this->tokens->getToken();

        if (null === $token) {
            return '';
        }

        if (! ($user = $token->getUser()) instanceof UserIdentity) {
            return '';
        }

        $now = new \DateTimeImmutable();
        $result = $this->calendar->byWeek($now, $user->getId());

        $dates = iterator_to_array(
            new \DatePeriod(
                $result->start,
                new \DateInterval('P1D'),
                $result->end
            )
        );

        return $twig
            ->render(
                'widget/work/projects/calendar/w_calendar.html.twig',
                [
                    'dates' => $dates,
                    'now' => $now,
                    'result' => $result,
                ]
            );
    }
}
