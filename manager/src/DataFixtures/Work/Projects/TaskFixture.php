<?php

declare(strict_types=1);

namespace App\DataFixtures\Work\Projects;

use Faker\Factory;
use Faker\Generator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Model\Work\Entity\Projects\Task\Task;
use App\Model\Work\Entity\Projects\Task\Type;
use Doctrine\Common\Persistence\ObjectManager;
use App\Model\Work\Entity\Projects\Task\Status;
use App\Model\Work\Entity\Members\Member\Member;
use App\Model\Work\Entity\Projects\Project\Project;
use App\Model\Work\Entity\Projects\Task\Id as TaskId;
use App\Model\Work\Entity\Projects\Project\Membership;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TaskFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Generator $faker */
        $faker = Factory::create();

        $projects = [
            $this->getReference(ProjectFixture::REFERENCE_FIRST),
            $this->getReference(ProjectFixture::REFERENCE_SECOND),
        ];

        $privious = [];

        $date = new \DateTimeImmutable('-180 days');

        for ($i = 0; $i < 100; $i++) {
            /** @var Project $project */
            $project = $faker->randomElement($projects);
            /** @var Member $actor */
            $actor = $faker->randomElement($project->getMemberships())->getMember();

            $task = $this->createRandomTask(new TaskId($i + 1), $project, $faker, $date);
            $date = $date->modify('+' . $faker->numberBetween(1, 3) . 'days 3minutes');

            if ($faker->boolean(40)) {
                $task->plan(
                    $actor,
                    $date,
                    $date->modify('+' . $faker->numberBetween(1, 30) . 'days')
                );
            }

            $memberships = $project->getMemberships();

            $randomMemberships = $faker->randomElements(
                $memberships,
                $faker->numberBetween(0, count($memberships))
            );

            foreach ($randomMemberships as $membership) {
                /** @var Membership $membership */
                $task->assignExecutor($actor, $date, $membership->getMember());
            }

            if ($faker->boolean(60)) {
                $task->changeProgress(
                    $actor,
                    $date->modify('+' . $faker->numberBetween(3, 6) . ' hours'),
                    $faker->randomElement([25, 50, 75])
                );

                $task->changeStatus(
                    $actor,
                    $date->modify('+' . $faker->numberBetween(1, 3) . ' days'),
                    new Status(
                        $faker->randomElement([
                            Status::WORKING,
                            Status::HEPL,
                            Status::CHECKING,
                            Status::REJECTED,
                            Status::DONE,
                        ])
                    )
                );
            }

            if ($faker->boolean()) {
                $task->changePriority(
                    $actor,
                    $date->modify('+' . $faker->numberBetween(1, 3) . ' days'),
                    $faker->randomElement(
                        array_diff([1, 2, 3, 4], [$task->getPriority()])
                    )
                );
            }

            if ($privious && $faker->boolean(30)) {
                $task->setChildOf(
                    $actor,
                    $date->modify('+' . $faker->numberBetween(1, 5) . ' days'),
                    $faker->randomElement($privious)
                );
            }

            $privious[] = $task;

            $manager->persist($task);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ProjectFixture::class,
        ];
    }

    private function createRandomTask(
        TaskId $id,
        Project $project,
        Generator $faker,
        \DateTimeImmutable $date
    ): Task {
        return new Task(
            $id,
            $project,
            $faker->randomElement($project->getMemberships())->getMember(),
            $date,
            trim($faker->sentence(random_int(2, 3)), '.'),
            $faker->paragraphs(3, true),
            new Type($faker->randomElement([Type::NONE, Type::FEATURE, Type::ERROR])),
            $faker->numberBetween(1, 4)
        );
    }
}
