<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Task\Change;

use Doctrine\ORM\Mapping as ORM;
use App\Model\Work\Entity\Projects\Task\Task;
use App\Model\Work\Entity\Members\Member\Member;

/**
 * @ORM\Entity
 * @ORM\Table(name="work_projects_task_changes")
 */
class Change
{
    /**
     * @var Task
     * @ORM\ManyToOne(targetEntity="App\Model\Work\Entity\Projects\Task\Task", inversedBy="changes")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @ORM\Id
     */
    private $task;
    /**
     * @var Id
     * @ORM\Column(type="work_projects_task_change_id")
     * @ORM\Id
     */
    private $id;
    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity="App\Model\Work\Entity\Members\Member\Member")
     * @ORM\JoinColumn(name="actor_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $actor;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;
    /**
     * @var Set
     * @ORM\Embedded(class="Set")
     */
    private $set;

    public function __construct(
        Task $task,
        Id $id,
        Member $actor,
        \DateTimeImmutable $date,
        Set $set
    ) {
        $this->task = $task;
        $this->id = $id;
        $this->actor = $actor;
        $this->date = $date;
        $this->set = $set;
    }

    /**
     * Get the value of id
     *
     * @return  Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Get the value of actor
     *
     * @return  Member
     */
    public function getActor(): Member
    {
        return $this->actor;
    }

    /**
     * Get the value of date
     *
     * @return  \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * Get the value of set
     *
     * @return  Set
     */
    public function getSet(): Set
    {
        return $this->set;
    }
}
