<?php

declare(strict_types=1);

namespace App\Event\Listener\Work\Projects\Task;

use phpcent\Client;
use App\Model\Work\Entity\Projects\Task\TaskRepository;
use App\Model\Work\Entity\Members\Member\MemberRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Model\Work\Entity\Projects\Task\Event\TaskExecutorAssigned;

class SiteNotificationSubscriber implements EventSubscriberInterface
{
    private $tasks;
    private $members;
    private $centrifugo;

    public function __construct(
        TaskRepository $tasks,
        MemberRepository $members,
        Client $centrifugo
    ) {
        $this->tasks = $tasks;
        $this->members = $members;
        $this->centrifugo = $centrifugo;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            TaskExecutorAssigned::class => [
                ['onTaskExecutorAssignedExecutor'],
                ['onTaskExecutorAssignedAuthor'],
            ]
        ];
    }

    public function onTaskExecutorAssignedExecutor(TaskExecutorAssigned $event): void
    {
        if ($event->executorId->isEqual($event->actorId)) {
            return;
        }

        $task = $this->tasks->get($event->taskId);
        $author = $task->getAuthor();
        $executor = $this->members->get($event->executorId);

        if ($executor === $author) {
            return;
        }

        $message =  'You were assigned to the task '
            . $task->getName()
            . ', author - '
            . $author->getName()->getFull();

        $channel = 'alerts#' . $executor->getId()->getValue();

        $this->centrifugo->publish($channel, $message);
    }

    public function onTaskExecutorAssignedAuthor(TaskExecutorAssigned $event): void
    {
        $task = $this->tasks->get($event->taskId);
        $author = $task->getAuthor();
        $executor = $this->members->get($event->executorId);

        if ($executor === $author) {
            return;
        }

        $message = 'There was assigned executor - '
            . $executor->getName()->getFull()
            . ' for your task: '
            . $task->getName()
        ;

        $channel = 'alerts#' . $author->getId()->getValue();

        $this->centrifugo->publish($channel, $message);
    }
}
