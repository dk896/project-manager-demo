<?php

declare(strict_types=1);

use App\Model\Work\Entity\Projects\Role\Permission;

return [
    Permission::MANAGE_PROJECT_MEMBERS => 'Manage project members',
    Permission::VIEW_TASKS => 'View tasks',
    Permission::MANAGE_TASKS => 'Manage tasks',
];
