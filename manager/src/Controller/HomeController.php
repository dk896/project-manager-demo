<?php

declare(strict_types=1);

namespace App\Controller;

use App\Annotation\RequiresUserCredits;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @RequiresUserCredits()
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('app/home.html.twig');
    }
}
