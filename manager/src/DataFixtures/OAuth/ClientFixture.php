<?php

declare(strict_types=1);

namespace App\DataFixtures\OAuth;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Trikoder\Bundle\OAuth2Bundle\Model\Grant;
use Doctrine\Common\Persistence\ObjectManager;
use Trikoder\Bundle\OAuth2Bundle\Model\Client;
use Trikoder\Bundle\OAuth2Bundle\OAuth2Grants;
use Trikoder\Bundle\OAuth2Bundle\Model\Scope;
use Trikoder\Bundle\OAuth2Bundle\Model\RedirectUri;

class ClientFixture extends Fixture
{
    public const OAUTH_CLIENT_ID = 'oauth';
    public const OAUTH_CLIENT_SECRET = 'secret';
    public const OAUTH_REDIRECT_URI = 'http://localhost:8080/docs/oauth2-redirect.html';

    public function load(ObjectManager $manager): void
    {
        $client = new Client(self::OAUTH_CLIENT_ID, self::OAUTH_CLIENT_SECRET);

        $client->setGrants(
            new Grant(OAuth2Grants::AUTHORIZATION_CODE),
            new Grant(OAuth2Grants::IMPLICIT),
            new Grant(OAuth2Grants::PASSWORD),
            new Grant(OAuth2Grants::CLIENT_CREDENTIALS),
            new Grant(OAuth2Grants::REFRESH_TOKEN)
        );

        $client->setScopes(new Scope('common'));
        $client->setRedirectUris(
            new RedirectUri(self::OAUTH_REDIRECT_URI)
        );

        $manager->persist($client);

        $manager->flush();
    }
}
