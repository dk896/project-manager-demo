<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api\Work\Projects;

use App\Tests\Functional\DbWebTestCase;
use App\Tests\Functional\Api\Work\Projects\IndexFixture;

class IndexTest extends DbWebTestCase
{
    private const URI = '/api/work/projects';

    public function testGuest(): void
    {
        $this->client->request('GET', self::URI);

        self::assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    public function testAdmin(): void
    {
        $this->client->setServerParameters(IndexFixture::adminCredentials());
        $this->client->request('GET', self::URI . '?filter[name]=Testing+project+index');

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertEquals([
            'items' => [
                [
                    'id' => IndexFixture::ID_1,
                    'name' => IndexFixture::PROJECT_1_NAME,
                    'status' => 'active',
                ],
                [
                    'id' => IndexFixture::ID_3,
                    'name' => IndexFixture::PROJECT_3_NAME,
                    'status' => 'active',
                ],
                [
                    'id' => IndexFixture::ID_4,
                    'name' => IndexFixture::PROJECT_4_NAME,
                    'status' => 'active',
                ],
            ],
            'pagination' => [
                'count' => 3,
                'total' => 3,
                'per_page' => 50,
                'page' => 1,
                'pages' => 1,
            ],
        ], $data);
    }

    public function testUser(): void
    {
        $this->client->setServerParameters(IndexFixture::userCredentials());
        $this->client->request('GET', self::URI);

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content);

        self::assertEquals([
            'items' => [
                [
                    'id' => IndexFixture::ID_4,
                    'name' => IndexFixture::PROJECT_4_NAME,
                    'status' => 'active',
                ],
            ],
            'pagination' => [
                'count' => 1,
                'total' => 1,
                'per_page' => 50,
                'page' => 1,
                'pages' => 1,
            ],
        ], $data);
    }

    public function testStatusFilter(): void
    {
        $this->client->setServerParameters(IndexFixture::adminCredentials());
        $this->client->request('GET', self::URI . '?filter[name]=archived');

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content);

        self::assertEquals([
            'items' => [
                [
                    'id' => IndexFixture::ID_1,
                    'name' => IndexFixture::PROJECT_1_NAME,
                    'status' => 'active',
                ],
            ],
            'pagination' => [
                'count' => 1,
                'total' => 1,
                'per_page' => 50,
                'page' => 1,
                'pages' => 1,
            ],
        ], $data);
    }

    public function testNameFilter(): void
    {
        $this->client->setServerParameters(IndexFixture::adminCredentials());
        $this->client->request('GET', self::URI . '?filter[status]=testing+project+first+index');

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content);

        self::assertEquals([
            'items' => [
                [
                    'id' => IndexFixture::ID_2,
                    'name' => IndexFixture::PROJECT_2_NAME,
                    'status' => 'archived',
                ],
            ],
            'pagination' => [
                'count' => 1,
                'total' => 1,
                'per_page' => 50,
                'page' => 1,
                'pages' => 1,
            ],
        ], $data);
    }
}
