<?php

declare(strict_types=1);

namespace App\Service;

class Gravatar
{
    const BASE_URL = '//www.gravatar.com/avatar/';

    public static function url(string $email, int $size): string
    {
        return self::BASE_URL
            . md5($email)
            . '?'
            . http_build_query([
                's' => $size,
                'd' => 'identicon'
            ]);
    }
}
