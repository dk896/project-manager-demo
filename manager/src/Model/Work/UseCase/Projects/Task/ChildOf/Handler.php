<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\ChildOf;

use App\Model\Flusher;
use App\Model\Work\Entity\Projects\Task\Id as TaskId;
use App\Model\Work\Entity\Projects\Task\TaskRepository;
use App\Model\Work\Entity\Members\Member\Id as MemberId;
use App\Model\Work\Entity\Members\Member\MemberRepository;

class Handler
{
    private $members;
    private $tasks;
    private $flusher;

    public function __construct(
        MemberRepository $members,
        TaskRepository $tasks,
        Flusher $flusher
    ) {
        $this->members = $members;
        $this->tasks = $tasks;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $actor = $this->members->get($command->actor);
        $task = $this->tasks->get(new TaskId($command->id));

        if ($command->parent) {
            $parent = $this->tasks->get(new TaskId($command->parent));

            $task->setChildOf($actor, new \DateTimeImmutable(), $parent);
        }

        if (!$command->parent) {
            $task->setRoot();
        }

        $this->flusher->flush($task);
    }
}
