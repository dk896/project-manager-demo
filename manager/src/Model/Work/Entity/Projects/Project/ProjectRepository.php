<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Project;

use App\Model\EntityNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\Work\Entity\Projects\Role\Id as RoleId;

class ProjectRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository(Project::class);
    }

    public function get(Id $id): Project
    {
        /** @var Project $project */
        if (!$project = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Project is not found.');
        }

        return $project;
    }

    public function add(Project $project): void
    {
        $this->em->persist($project);
    }

    public function remove(Project $project): void
    {
        $this->em->remove($project);
    }

    public function hasMembersWithRole(RoleId $roleId): bool
    {
        return $this
            ->repo
            ->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->innerJoin('p.memberships', 'ms')
            ->innerJoin('ms.roles', 'r')
            ->andWhere('r.id = :role_id')
            ->setParameter(':role_id', $roleId->getValue())
            ->getQuery()
            ->getSingleScalarResult()
                > 0;
    }
}
