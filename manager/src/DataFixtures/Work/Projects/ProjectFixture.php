<?php

declare(strict_types=1);

namespace App\DataFixtures\Work\Projects;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Model\Work\Entity\Projects\Role\Role;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\Work\Projects\RoleFixture;
use App\DataFixtures\Work\Members\MemberFixture;
use App\Model\Work\Entity\Members\Member\Member;
use App\Model\Work\Entity\Projects\Project\Project;
use App\Model\Work\Entity\Projects\Project\Id as ProjectId;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Model\Work\Entity\Projects\Project\Department\Id as DepartmentId;

class ProjectFixture extends Fixture implements DependentFixtureInterface
{
    public const REFERENCE_FIRST = 'first_project';
    public const REFERENCE_SECOND = 'second_project';

    public function load(ObjectManager $manager): void
    {
        /** @var Member $admin */
        $admin = $this->getReference(MemberFixture::REFERENCE_ADMIN);

        /** @var Member $user */
        $user = $this->getReference(MemberFixture::REFERENCE_USER);

        /** @var Role $roleManager */
        $roleManager = $this->getReference(RoleFixture::REFERENCE_MANAGER);

        /** @var Role $roleGuest */
        $roleGuest = $this->getReference(RoleFixture::REFERENCE_GUEST);

        $active = $this->createProject('First project', 1);

        $active->addDepartment($developmentId = DepartmentId::next(), 'Web development');
        $active->addDepartment($marketingId = DepartmentId::next(), 'Marketing');

        $active->addMember($admin, [$developmentId], [$roleManager]);
        $active->addMember($user, [$marketingId], [$roleGuest]);

        $manager->persist($active);
        $this->setReference(self::REFERENCE_FIRST, $active);

        $active2 = $this->createProject('Second project', 2);

        $active2->addDepartment(
            $mobileDepartmentId = DepartmentId::next(),
            'Mobile development'
        );

        $active2->addDepartment(
            $researchDepartmentId = DepartmentId::next(),
            'Research'
        );

        $active2->addMember($admin, [$mobileDepartmentId, $researchDepartmentId], [$roleManager]);
        $active2->addMember($user, [$researchDepartmentId], [$roleGuest]);

        $manager->persist($active2);

        $this->setReference(self::REFERENCE_SECOND, $active2);

        $archived = $this->createArchivedProject('Third project', 3);
        $manager->persist($archived);

        $manager->flush();
    }

    private function createArchivedProject(string $name, int $sort): Project
    {
        $project = $this->createProject($name, $sort);
        $project->archive();

        return $project;
    }

    private function createProject(string $name, int $sort): Project
    {
        return new Project(
            ProjectId::next(),
            $name,
            $sort
        );
    }

    public function getDependencies(): array
    {
        return [
            MemberFixture::class,
            RoleFixture::class,
        ];
    }
}
