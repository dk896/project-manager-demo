<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\Files\Remove;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    public $actor;
    /**
     * @Assert\NotBlank()
     * @property int $id - Value of Task Id
     */
    public $id;
    /**
     * @Assert\NotBlank()
     * @property string $file - Value of File Id
     */
    public $file;

    public function __construct(
        string $actor,
        int $taskIdValue,
        string $fileIdValue
    ) {
        $this->actor = $actor;
        $this->id = $taskIdValue;
        $this->file = $fileIdValue;
    }
}
