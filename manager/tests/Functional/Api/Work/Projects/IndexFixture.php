<?php

declare (strict_types = 1);

namespace App\Tests\Functional\Api\Work\Projects;

use App\Model\User\Entity\User\Role;
use App\Model\User\Entity\User\User;
use App\Tests\Functional\AuthFixture;
use App\Tests\Builder\User\UserBuilder;
use App\Model\User\Service\PasswordHasher;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Model\Work\Entity\Projects\Project\Id;
use Doctrine\Common\Persistence\ObjectManager;
use App\Tests\Builder\Work\Members\GroupBuilder;
use App\Tests\Builder\Work\Projects\RoleBuilder;
use App\Tests\Builder\Work\Members\MemberBuilder;
use App\Model\User\Entity\User\Email as UserEmail;
use App\Model\Work\Entity\Projects\Project\Project;
use App\Model\Work\Entity\Projects\Role\Permission;
use App\Model\Work\Entity\Members\Member\Id as MemberId;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Model\Work\Entity\Members\Member\Email as MemberEmail;
use App\Model\Work\Entity\Projects\Project\Department\Id as DepartmentId;

class IndexFixture extends Fixture
{
    public const ID_1 = '10000000-0000-0000-0000-000000000001';
    public const ID_2 = '10000000-0000-0000-0000-000000000002';
    public const ID_3 = '10000000-0000-0000-0000-000000000003';
    public const ID_4 = '10000000-0000-0000-0000-000000000004';

    public const PROJECT_1_NAME = 'Testing project first index';
    public const PROJECT_2_NAME = 'Testing project second index';
    public const PROJECT_3_NAME = 'Testing project third index';
    public const PROJECT_4_NAME = 'Testing project fourth index';

    public const USER_IDX_EMAIL = 'user-projects-index@app.org';
    public const ADMIN_IDX_EMAIL = 'user-projects-index@app.org';

    private $hasher;

    public function __construct(PasswordHasher $hasher)
    {
        $this->hasher = $hasher;
    }

    public static function userCredentials(): array
    {
        return [
            'PHP_AUTH_USER' => self::USER_IDX_EMAIL,
            'PHP_AUTH_PW' => 'password',
        ];
    }

    public static function adminCredentials(): array
    {
        return [
            'PHP_AUTH_USER' => self::ADMIN_IDX_EMAIL,
            'PHP_AUTH_PW' => 'password',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $hash = $this->hasher->hash('password');

        $user = (new UserBuilder())
            ->viaEmail(new UserEmail(self::USER_IDX_EMAIL), $hash)
            ->confirmed()
            ->build();

        $manager->persist($user);

        $admin = (new UserBuilder())
            ->viaEmail(new UserEmail(self::ADMIN_IDX_EMAIL), $hash)
            ->confirmed()
            ->withRole(Role::admin())
            ->build();

        $manager->persist($admin);

        $group = (new GroupBuilder())
            ->withName('Testing staff')
            ->build();

        $manager->persist($group);

        $userMember = (new MemberBuilder())
            ->withId(new MemberId($user->getId()->getValue()))
            ->withEmail(new MemberEmail($user->getEmail()->getValue()))
            ->build($group);

        $manager->persist($userMember);

        $developerRole = (new RoleBuilder())
            ->withName('Developers index')
            ->withPermissions([Permission::MANAGE_TASKS])
            ->build();

        $manager->persist($developerRole);

        $project = new Project(new Id(self::ID_1), self::PROJECT_1_NAME, 1);
        $manager->persist($project);

        $project = new Project(new Id(self::ID_2), self::PROJECT_2_NAME, 2);
        $project->addDepartment($departmentId = DepartmentId::next(), 'Web development');
        $project->addMember($userMember, [$departmentId], [$developerRole]);
        $project->archive();
        $manager->persist($project);

        $project = new Project(new Id(self::ID_3), self::PROJECT_3_NAME, 3);
        $manager->persist($project);

        $project = new Project(new Id(self::ID_4), self::PROJECT_4_NAME, 4);
        $project->addDepartment($departmentId = DepartmentId::next(), 'Mobile development');
        $project->addMember($userMember, [$departmentId], [$developerRole]);
        $manager->persist($project);

        $manager->flush();
    }
}
