<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\User\Entity\User\Reset;

use PHPUnit\Framework\TestCase;
use App\Tests\Builder\User\UserBuilder;
use App\Model\User\Entity\User\ResetToken;

class ResetTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = (new UserBuilder())
            ->viaEmail()
            ->confirmed()
            ->build();

        $now = new \DateTimeImmutable();
        $token = new ResetToken('token', $now->modify('+1 day'));

        $user->requestPasswordReset($token, $now);

        self::assertNotNull($user->getResetToken());

        $user->passwordReset($now, $hash = 'hash');

        self::assertNull($user->getResetToken());
        self::assertEquals($hash, $user->getPasswordHash());
    }

    public function testExpiredToken(): void
    {
        $user = (new UserBuilder())
            ->viaEmail()
            ->confirmed()
            ->build();

        $now = new \DateTimeImmutable();
        $token = new ResetToken('token', $now);

        $user->requestPasswordReset($token, $now);

        $this->expectExceptionMessage('Reset token expired.');
        $user->passwordReset($now->modify('+1 day'), 'hash');
    }

    public function testNotRequested(): void
    {
        $user = (new UserBuilder())
            ->viaEmail()
            ->confirmed()
            ->build();

        $now = new \DateTimeImmutable();

        $this->expectExceptionMessage('Resetting was not requested.');
        $user->passwordReset($now, 'hash');
    }
}
