<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\Network\Attach;

use App\Model\Flusher;
use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\User;
use App\Model\User\Entity\User\UserRepository;

class Handler
{
    /** @var UserRepository */
    private $users;

    /** @var Flusher */
    private $flusher;

    public function __construct(
        UserRepository $users,
        Flusher $flusher
    ) {
        $this->users = $users;
        $this->flusher = $flusher;
    }

    /**
     * Method for handle network's command DTO
     *
     * @param Command $command DTO
     * @return void
     **/
    public function handle(Command $command): void
    {
        if (
            $this->users->hasByNetworkIdentity(
                $command->network,
                $command->identity
            )
        ) {
            throw new \DomainException('Profile already in use.');
        }

        $user = $this->users->get(new Id($command->user));

        $user->attachNetwork(
            $command->network,
            $command->identity
        );

        $this->flusher->flush();
    }
}
