<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\Executor\Assign;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    public $actor;
    /**
     * Task`s value of Id
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * Members values of Id array
     * @var array
     * @Assert\NotBlank()
     */
    public $members;

    public function __construct(string $actor, int $id)
    {
        $this->actor = $actor;
        $this->id = $id;
    }
}
