<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\Files\Add;

use Symfony\Component\Validator\Constraints as Assert;
use App\Model\Work\Entity\Projects\Task\File\File;

class Command
{
    /**
     * @Assert\NotBlank()
     * Value of Member Id
     */
    public $actor;
    /**
     * @Assert\NotBlank()
     * Value of Task Id
     */
    public $id;
    /**
     * @var File[]
     */
    public $files;

    public function __construct(string $actor, int $taskIdValue)
    {
        $this->id = $taskIdValue;
        $this->actor = $actor;
    }
}
