<?php

declare(strict_types=1);

namespace App\Security;

use App\Model\User\Entity\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class UserIdentity implements UserInterface, EquatableInterface
{
    private $id;
    private $username;
    private $password;
    private $displayingName;
    private $role;
    private $status;

    public function __construct(
        string $id,
        string $username,
        string $password,
        string $displayingName,
        string $role,
        string $status
    ) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->displayingName = $displayingName;
        $this->role = $role;
        $this->status = $status;
    }

    /**
     * Get the value of user's id
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Check user's status
     */
    public function isActive(): bool
    {
        return $this->status === User::STATUS_ACTIVE;
    }

    public function getDisplayingName(): string
    {
        return $this->displayingName;
    }

    /**
     * Get the value of username
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Get the value of password
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Get the values of roles
     */
    public function getRoles(): array
    {
        return [$this->role];
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {}

    public function isEqualTo(UserInterface $user): bool
    {
        if (! $user instanceof self) {
            return false;
        }

        return
            $this->id === $user->id
            && $this->password === $user->password
            && $this->role === $user->role
            && $this->status === $user->status
        ;
    }
}
