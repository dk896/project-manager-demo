<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\User\Entity\User;

use PHPUnit\Framework\TestCase;
use App\Tests\Builder\User\UserBuilder;
use App\Model\User\Entity\User\Role;

class RoleTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = (new UserBuilder())
            ->viaEmail()
            ->confirmed()
            ->build();

        $user->changeRole(Role::admin());

        self::assertFalse($user->getRole()->isUser());
        self::assertTrue($user->getRole()->isAdmin());
    }

    public function testAlready(): void
    {
        $user = (new UserBuilder())
            ->viaEmail()
            ->confirmed()
            ->build();

        $this->expectExceptionMessage('Given role equal the previous.');

        $user->changeRole(Role::user());
    }
}
