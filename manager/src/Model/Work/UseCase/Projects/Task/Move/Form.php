<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Task\Move;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\ReadModel\Work\Projects\Project\ProjectFetcher;

class Form extends AbstractType
{
    private $projects;

    public function __construct(ProjectFetcher $projects)
    {
        $this->projects = $projects;
    }

    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add(
                'project',
                Type\ChoiceType::class,
                [
                    'choices' => array_flip($this->projects->allList()),
                ]
            )
            ->add(
                'withChildren',
                Type\CheckboxType::class,
                [
                    'required' => false,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}
