<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\User\Entity\User\Network;

use PHPUnit\Framework\TestCase;
use App\Model\User\Entity\User\Id;
use App\Model\User\Entity\User\User;
use App\Model\User\Entity\User\Network;
use App\Model\User\Entity\User\Name;

class AuthTest extends TestCase
{
    public function testSuccess(): void
    {
        $user = User::signUpByNetwork(
            $id = Id::next(),
            $date = new \DateTimeImmutable(),
            $name = new Name('John', 'Doe'),
            $network = 'vk',
            $identity = '00001'
        );

        self::assertTrue($user->isActive());

        self::assertEquals($id, $user->getId());
        self::assertEquals($date, $user->getDate());
        self::assertEquals($name, $user->getName());

        self::assertCount(1, $networks = $user->getNetworks());
        self::assertInstanceOf(Network::class, $first = reset($networks));

        /** @var Network $first */
        self::assertEquals($network, $first->getNetwork());
        self::assertEquals($identity, $first->getIdentity());

        self::assertTrue($user->getRole()->isUser());
    }
}
