<?php

declare(strict_types=1);

namespace App\Model\Work\UseCase\Projects\Project\Department\Edit;

use App\Model\Work\Entity\Projects\Project\Project;
use Symfony\Component\Validator\Constraints as Assert;
use App\Model\Work\Entity\Projects\Project\Department\Department;

class Command
{
    /**
     * @Assert\NotBlank()
     * project id (string)
     */
    public $project;

    /**
     * @Assert\NotBlank()
     * department id (string)
     */
    public $id;

    /**
     * @Assert\NotBlank()
     */
    public $name;

    public function __construct(string $project, string $id)
    {
        $this->project = $project;
        $this->id = $id;
    }

    public static function fromDepartment(
        Project $project,
        Department $department
    ): self {
        $command = new self(
            $project->getId()->getValue(),
            $department->getId()->getValue()
        );

        $command->name = $department->getName();

        return $command;
    }
}
