<?php

declare(strict_types=1);

namespace App\Tests\Builder\Work\Projects;

use App\Model\Work\Entity\Projects\Project\Project;
use App\Model\Work\Entity\Projects\Project\Id as ProjectId;

class ProjectBuilder
{
    private $name;
    private $sort;

    public function __construct()
    {
        $this->name = 'Serious project';
        $this->sort = 1;
    }

    public function withName(string $name): self
    {
        $clone = clone $this;
        $clone->name = $name;

        return $clone;
    }

    public function build(): Project
    {
        return new Project(
            ProjectId::next(),
            $this->name,
            $this->sort
        );
    }
}
