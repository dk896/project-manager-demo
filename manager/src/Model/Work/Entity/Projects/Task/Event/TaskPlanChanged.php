<?php

declare(strict_types=1);

namespace App\Model\Work\Entity\Projects\Task\Event;

use App\Model\Work\Entity\Projects\Task\Id as TaskId;
use App\Model\Work\Entity\Members\Member\Id as MemberId;

class TaskPlanChanged
{
    public $actorId;
    public $taskId;
    public $planDate;

    public function __construct(
        MemberId $actorId,
        TaskId $taskId,
        ?\DateTimeImmutable $planDate
    ) {
        $this->actorId = $actorId;
        $this->taskId = $taskId;
        $this->planDate = $planDate;
    }
}
