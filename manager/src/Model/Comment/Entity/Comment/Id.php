<?php

declare(strict_types=1);

namespace App\Model\Comment\Entity\Comment;

use Webmozart\Assert\Assert;
use Ramsey\Uuid\Uuid;

class Id
{
    private $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value);
        $this->value = $value;
    }

    public static function next(): self
    {
        return new self(Uuid::uuid4()->toString());
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isEqualTo(self $otherId): bool
    {
        return $this->getValue() === $otherId->getValue();
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
